<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Show</title>

    {{-- Bootstrap CSS --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    {{-- Bootstrap Javascript --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container my-5">
        <h1 class="d-flex justify-content-center">Show User</h1>

        <div class="row">
            <div class="pull-right">
                <a class="btn btn-dark" href="{{ route('users.index') }}">Index</a>
                {{-- <a class="btn btn-secondary" href="{{ url()->previous() }}"> Back</a> --}}
            </div>
        </div>
       
        <div class="container w-50 p-3">
            <table class="table table-dark table-bordered table-hover">
                <tr>
                    <th>S.N</th>
                    <td>{{ $user->id }}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <th>Age</th>
                    <td>{{ $user->age }}</td>
                </tr>
                <tr>
                    <th>Image</th>
                    <td><img src="{{ asset('blog/users/'.$user->image) }}" width="70px" height="70px" alt=""></td>
                </tr>
                <tr>
                    <th>Bio</th>
                    <td>{{ $user->bio }}</td>
                </tr>
            </table>
            <div class="row">
                <div class="pull-right">
                    <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                        <a class="btn btn-sm btn-primary" href="{{ route('users.posts.index',$user->id) }}">Posts</a>
                        <a class="btn btn-sm btn-warning" href="{{ route('users.edit',$user->id) }}">Edit</a>
    
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>        

    </div>
</body>
</html>