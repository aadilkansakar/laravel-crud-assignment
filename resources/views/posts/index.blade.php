<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Post Index</title>

    {{-- Bootstrap CSS --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    {{-- Bootstrap Javascript --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container my-5">        
        <h1 class="d-flex justify-content-center">Posts: {{ $user->posts->count() }}</h1>
            
        <div class="container" id="dash-txt">    
            <!-- Create Button -->
            <a class="btn btn-success float-end" href ="{{ route('users.posts.create',$user->id) }}">
                Create Post
            </a>
            <a class="btn btn-dark float-start" href="{{ route('users.index') }}">User Index</a>
        </div>


        <!-- Table -->
        <div class="container my-5">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">S.N</th>
                        <th scope="col">Title</th>
                        <th scope="col">Image</th>
                        <th scope="col">Description</th>
                        <th scope="col">Status</th>
                        <th scope="col">Position</th>
                        <th scope="col">Categories</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</td>
                        <td><a href="{{ route('users.posts.show',[$user->id, $post->id]) }}">{{ $post->title }}</a></td>
                        <td>
                            <img src="{{ asset('blog/posts/'.$post->image) }}" width="70px" height="70px" alt="">
                        </td>
                        <td>{{ $post->description }}</td>
                        <td>{{ $post->status }}</td>
                        <td>{{ $post->position }}</td>
                        <td>
                            @foreach ($post->categories as $category)
                                {{ $category->title }}
                            @endforeach                    
                        </td>
                        <td>
                            <form action="{{ route('users.posts.destroy',[$user->id, $post->id]) }}" method="POST" class="d-flex justify-content-evenly">
                                <a class="btn btn-sm btn-warning" href="{{ route('users.posts.edit',[$user->id,$post->id]) }}">Edit</a>

                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>                        
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- Table End -->

            
        @if ($message = Session::get('success'))
            <div class="container alert alert-success alert-dismissible fade show">
                <p>{{ $message }}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>            
        @endif
    </div>
</body>
</html>