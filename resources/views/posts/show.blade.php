<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Post Show</title>

    {{-- Bootstrap CSS --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    {{-- Bootstrap Javascript --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container my-5">
        <h1 class="d-flex justify-content-center">Show Post</h1>

        <div class="row">
            <div class="pull-right">
                <a class="btn btn-dark" href="{{ route('users.posts.index', $user->id) }}">Index</a>
                {{-- <a class="btn btn-secondary" href="{{ url()->previous() }}"> Back</a> --}}
            </div>
        </div>
       
        <div class="container w-50 p-3">
            <table class="table table-dark table-bordered table-hover">
                <tr>
                    <th>S.N</th>
                    <td>{{ $post->id }}</td>
                </tr>
                <tr>
                    <th>Title</th>
                    <td>{{ $post->title }}</td>
                </tr>
                <tr>
                    <th>Image</th>
                    <td>
                        <img src="{{ asset('blog/posts/'.$post->image) }}" width="70px" height="70px" alt="">
                    </td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>{{ $post->description }}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{ $post->status }}</td>
                </tr>
                <tr>
                    <th>Position</th>
                    <td>{{ $post->position }}</td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td>
                        @foreach ($post->categories as $category)
                        {{ $category->title }}
                        @endforeach
                    </td>
                </tr>
            </table>
            <div class="row">
                <div class="pull-right">
                    <form action="{{ route('users.posts.destroy',[$user->id, $post->id]) }}" method="POST">
                        <a class="btn btn-sm btn-warning" href="{{ route('users.posts.edit',[$user->id, $post->id]) }}">Edit</a>
    
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>        

    </div>
</body>
</html>