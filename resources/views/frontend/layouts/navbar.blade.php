<body>
       
    <div class="tm-header">
        <div class="container-fluid">
            <div class="tm-header-inner">
                <a href="#" class="navbar-brand tm-site-name">Classic</a>
                
                <!-- navbar -->
                <nav class="navbar tm-main-nav">

                    <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#tmNavbar">
                        &#9776;
                    </button>
                    
                    <div class="collapse navbar-toggleable-sm" id="tmNavbar">
                        <ul class="nav navbar-nav">
                            <li class="nav-item {{ (request()->segment(1) == '') ? 'active' : '' }}">
                                <a href="{{ route('frontend.home') }}" class="nav-link">Home</a>
                            </li>
                            <li class="nav-item {{ (request()->segment(1) == 'categories') ? 'active' : '' }}">
                                <a href="{{ route('frontend.allcategories') }}" class="nav-link">Categories</a>
                            </li>
                            @foreach ($navitems as $category)
                            <li class="nav-item {{ (request()->segment(2) == $category->slug) ? 'active' : '' }}">
                                <a href="{{ route('frontend.category',$category->slug) }}" class="nav-link">{{ $category->title }}</a>
                            </li>
                            @endforeach
                        </ul>                        
                    </div>
                    
                </nav>  

            </div>                                  
        </div>            
    </div>
