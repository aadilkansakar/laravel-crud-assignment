@extends('frontend.layouts.app')

@section('title', 'Home')

@section('content')
<div class="tm-contact-img-container">
            
</div>

<section class="tm-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-xs-center">
                <h2 class="tm-gold-text tm-title">Introduction</h2>
                <p class="tm-subtitle">Suspendisse ut magna vel velit cursus tempor ut nec nunc. Mauris vehicula, augue in tincidunt porta, purus ipsum blandit massa.</p>
            </div>
        </div>
        <div class="row">

            @foreach ($posts as $post)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="tm-content-box">
                        <img src="{{ asset('blog/posts/'.$post->image) }}" alt="Image" class="tm-margin-b-20 img-fluid">
                        <h4 class="tm-margin-b-20 tm-gold-text">{{ $post->title }}</h4>
                        <p class="tm-margin-b-20">{{ Str::limit($post->description, 50) }}</p>
                        
                        @foreach($post->categories as $category)
                            <a href="{{ route('frontend.post.detail',[$category->slug, $post]) }}" class="tm-btn text-uppercase">Detail</a>    
                        @endforeach
                    </div>  

                </div>
            @endforeach

        </div> <!-- row -->
    </div>
</section>
@endsection