@extends('frontend.layouts.app')

@section('title', 'All Categories')

@section('content')

<div class="tm-about-img-container">
            
</div>

<section class="tm-section">
    <div class="container-fluid">
        <div class="row tm-margin-t-big">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="tm-2-col-right">
                    <div class="tm-2-rows-md-swap">
                        <div class="tm-overflow-auto row tm-2-rows-md-down-2">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <h3 class="tm-gold-text tm-title">
                                    Categories
                                </h3>
                                <nav>
                                    <ul class="nav">
                                        @forelse ($categories as $category)
                                            <li><a href="{{ route('frontend.category',$category->slug) }}" class="tm-text-link">{{ $category->title }}</a></li>
                                        @empty
                                            <div class="tm-content-box">
                                                <h4 class="tm-margin-b-20 tm-text">There are no categories active</h4>
                                            </div>  
                                        @endforelse
                                    </ul>
                                </nav>    
                            </div> <!-- col -->
                        </div>                        
                    </div>
                </div>                
            </div>
        </div> <!-- row -->
    </div>
</section>
@endsection