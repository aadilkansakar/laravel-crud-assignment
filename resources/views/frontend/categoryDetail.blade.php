@extends('frontend.layouts.app')

@section('title', 'Category-Detail')

@section('content')

<div class="tm-blog-img-container">
            
</div>

<section class="tm-section">
    <div class="container-fluid">
        <div class="row">
                <div class="row tm-margin-t-big">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-xs-center">
                            <h2 class="tm-gold-text tm-title">{{ $category->title }}</h2>
                        </div>
                    </div>

                    @forelse ($category_posts as $post)
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                            <div class="tm-content-box">
                                <img src="{{ asset('blog/posts/'.$post->image) }}" alt="Image" class="tm-margin-b-30 img-fluid">
                                <h4 class="tm-margin-b-20 tm-gold-text">{{ $post->title }}</h4>
                                <p class="tm-margin-b-20">{{ Str::limit($post->description, 50) }}</p>
                                <a href="{{ route('frontend.post.detail',[$category->slug, $post]) }}" class="tm-btn text-uppercase">Detail</a>    
                            </div>  
                        </div>
                    @empty
                        <div class="tm-content-box">
                            <h4 class="tm-margin-b-20 tm-text">There are no posts in this category</h4>
                        </div>  
                    @endforelse
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection