@extends('frontend.layouts.app')

@section('title', 'Post-Detail')

@section('content')
<div class="tm-about-img-container">
            
</div>

<section class="tm-section">
  <div class="container-fluid">
      <div class="row">

          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
              <div class="tm-blog-post">
                  <h1 class="tm-gold-text"></h1>
                  <h3 class="tm-gold-text">{{ $post->title }}</h3>
                  <p><b>Written By: </b>{{ $post->user->name }}</p>
                  <p><b>Category: </b>
                    @foreach ($post->categories as $category)
                        <a href="{{ route('frontend.category',$category->slug) }}">{{ $category->title }}</a>
                    @endforeach
                    </p>
                  <img src="{{ asset('blog/posts/'.$post->image) }}" alt="Image" class="img-fluid tm-img-post">
                  
                  <p>{{ $post->description }}</p>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection