<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {   
        $posts = Post::where('user_id', $user->id)->get();

        return view('posts.index', compact('user', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user, Category $category)
    {
        $categories = Category::where('status', 'active')->get();

        return view('posts.create', compact('user','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\PostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request, User $user, Post $post)
    {
        // Renaming and Storing Post image file in public
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = time().'.'.$extension;
            $file->move('blog/posts/', $fileName);
        }

        $post = Post::create($request->safe()->except(['image']) + ['user_id' => $user->id, 'image' => $fileName]);
        
        $category = $request->input('category');
        $post->categories()->attach($category);

        return redirect()->route('users.posts.index', $user)->with('success','Post Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Post $post)
    {
        return view('posts.show', compact('user','post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Post $post, Category $category)
    {
        $categories = Category::where('status', 'active')->get();

        return view('posts.edit', compact('user','post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\PostRequest  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request,User $user, Post $post)
    {
        // Destination of Posts image file in public for deletion
        if ($request->hasFile('image')) {
            if ($post->image) {
                $destination = 'blog/posts/'.$post->image;
                if (File::exists($destination)) {
                    unlink($destination);
                }
            }   
    
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = time().'.'.$extension;
            $file->move('blog/posts/', $fileName);
            $post->update($request->safe()->except(['image']) + ['image' => $fileName]);
        }
        else{
            $post->update($request->validated());
        }      

        $category = $request->input('category');
        $post->categories()->sync($category);

        return redirect()->route('users.posts.index', $user)->with('success','Post Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Post $post)
    {
        // Destination of Posts image file in public for deletion
        $destination = 'blog/posts/'.$post->image;
        if (File::exists($destination)) {
            unlink($destination);
        }

        $post->delete();

        return redirect()->route('users.posts.index', $user)->with('success', 'Post Deleted Successfully');
    }
}
