<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {   

        // Renamed User image file and stored in public
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = time().'.'.$extension;
            $file->move('blog/users/', $fileName);
        }

        User::create($request->safe()->except(['image']) + ['image' => $fileName]);

        return redirect()->route('users.index')->with('success','User Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        // Destination of User image file in public for deletion
        if ($request->hasFile('image')) {
            if ($user->image) {
                $destination = 'blog/users/'.$user->image;
                if (File::exists($destination)) {
                    unlink($destination);
                }
            }

            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = time().'.'.$extension;
            $file->move('blog/users/', $fileName);
            $user->update($request->safe()->except(['image']) + ['image' => $fileName]);
        }
        else
        {
            $user->update($request->validated());
        }


        return redirect()->route('users.index')->with('success','User Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // Deletion of posts images linked with user
        foreach ($user->posts as $post) {
            $destination_post = 'blog/posts/'.$post->image;
            if (File::exists($destination_post)) {
                unlink($destination_post);
            }

            $post->delete();
        }

        // Destination of User image file in public for deletion
        $destination_user = 'blog/users/'.$user->image;
        if (File::exists($destination_user)) {
            unlink($destination_user);
        }

        $user->delete();

        return redirect()->route('users.index')->with('success','User Deleted Successfully');
    }
}
