<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;

class FrontendController extends Controller
{
    public function getHome(Category $category)
    {
        $posts = Post::where('status', '=', 'active')->orderBy('position', 'asc')->limit(4)->get();
        
        return view('frontend.home', compact('posts'));
    }

    public function getPostDetail(Category $category, Post $post)
    {
        return view('frontend.postDetail', compact('post'));
    }

    public function getCategoryDetail(Category $category)
    {
        $category_posts = $category->posts()->where('status', '=', 'active')->orderBy('position', 'asc')->get();

        return view('frontend.categoryDetail', compact('category', 'category_posts'));
    }

    public function getCategoryAll(Category $category)
    {
        return view('frontend.categoryAll');
    }
}
