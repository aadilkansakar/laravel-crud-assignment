<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:200',
            'age' => 'required|numeric|min:18|max:125',            
            'bio' => 'nullable|max:500'
        ];
        
        if (request()->isMethod('POST')) {
            $rules = array_merge($rules + ['image' => 'required|image|mimes:png,jpg|max:2048',]);
        }
        elseif (request()->isMethod('PUT')) {
            $rules = array_merge($rules + ['image' => 'image|mimes:png,jpg|max:2048',]);
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Your name is required',
            'age.min' => 'You must atleast be 18 to participate',
            'image.max' => 'File needs to be under 2MB',
            'image.image' => 'File should be png or jpg'
        ];
    }
}
