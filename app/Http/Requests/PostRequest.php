<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:200',            
            'description' => 'nullable|max:500',
            'status' => 'required',
            'position' => 'required|numeric',
            'category' => 'required',
        ];

        if (request()->isMethod('POST')) {
            $rules = array_merge($rules + ['image' => 'required|image|max:2048|mimes:png,jpg']);
        }
        elseif (request()->isMethod('PUT')) {
            $rules = array_merge($rules + ['image' => 'image|max:2048|mimes:png,jpg']);
        }

        return $rules;
    }
}
