<?php

namespace App\Providers;

use App\Http\View\Composers\CategoryComposer;
use App\Http\View\Composers\NavbarComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            'frontend.layouts.navbar', NavbarComposer::class
        );

        View::composer(
            ['frontend.postDetail', 'frontend.categoryDetail', 'frontend.categoryAll'],
            CategoryComposer::class
        );
    }
}
