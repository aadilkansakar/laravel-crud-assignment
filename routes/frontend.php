<?php

use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\Route;

    Route::get('/', [FrontendController::class, 'getHome'])->name('frontend.home');

    Route::get('categories/all', [FrontendController::class, 'getCategoryAll'])->name('frontend.allcategories');
    
    Route::get('category/{category:slug}', [FrontendController::class, 'getCategoryDetail'])->where('category:slug', '[a-z]+')->name('frontend.category');
    
    Route::get('category/{category:slug}/{post}/postDetail', [FrontendController::class, 'getPostDetail'])->where('category:slug', '[a-z]+')->name('frontend.post.detail');

?>