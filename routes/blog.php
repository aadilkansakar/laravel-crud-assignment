<?php

use Illuminate\Support\Facades\Route;

Route::get('dashboard', function() {
    return redirect('dashboard/users');
});

Route::group(['namespace' => 'Blog', 'prefix' => 'dashboard'], function() {
    Route::resource('users', 'UserController');
    Route::resource('users.posts', 'PostController');
    Route::resource('category', 'CategoryController');
});

?>